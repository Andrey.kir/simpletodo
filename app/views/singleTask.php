<div id="user-page">
    <p class="breadcrumbs"><a href="/tasks">Назад</a></p>
    <div class="task-info-container">
        <?php if ($data) { ?>
            <h2><?php echo $data['title'];?></h2>
            <p><?php echo $data['description'];?></p>
            <?php if ($data['status'] == '1') { ?>
                <h2>Задача выполнена</h2>
            <?} else {?>
                <form action="/tasks/<? echo $data['ID'];?>" method="post">
                    <input type="hidden" name="via" value="done">
                    <input type="submit" value="Готово"/>
                </form>
            <?php }?>
        <?php } else {?>
            <h2>Задачи с таким ID не существует</h2>
        <?php }?>
    </div>
</div>