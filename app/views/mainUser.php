<div id="user-page">
    <h2>TODO LIST</h2>
    <div class="task-container">
        <?php if (empty ($data)) { ?>
            <h3>Все задачи выполнены</h3>
        <?php } ?>
        <?php foreach ($data AS $task) { ?>
            <article>
                <h4><?php echo $task['cat_name']; ?></h4>
                <h3><?php echo $task['title']; ?></h3>
                <div class="single-task-button">
                    <a href="<?php echo '/tasks/' . $task['ID']; ?>">Перейти к Задаче</a>
                </div>
            </article>
        <?php } ?>
    </div>
    <div class="logout-container">
        <form action="/" method="post">
            <input type="hidden" name="via" value="logout">
            <input type="submit" value="Выйти"/>
        </form>
    </div>
</div>