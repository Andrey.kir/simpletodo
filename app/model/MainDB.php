<?php

namespace App\Model;

use App\Core\DB;

class MainDB extends DB
{
    /**
     * @var
     */
    private $db;

    public function __construct()
    {
        $dbcon = new parent();
        $this->db = $dbcon->connect();
    }

    /**
     * @return mixed
     */
    public function checkUser()
    {
        $login = $_POST['name'];
        $pass  = $_POST['password'];
        $query = "SELECT privilege FROM users WHERE name = '" . $login . "' AND password='" . $pass . "'";
        $check = $this->db->prepare($query );
        $check->execute();

        return $check->fetchColumn();
    }

    /**
     * @param $data
     * @return bool
     */
    public function addTask($data)
    {
        $query = "INSERT INTO tasks SET cat_name=:cat_name, dateGMT=:dateGMT, title=:title, description=:description, status = 0";
        $check = $this->db->prepare($query);
        $check->execute($data);

        return false;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function checkDuplicate($data)
    {
        $title = $data['title'];
        $dateGMT = $data['dateGMT'];

        $query = "SELECT title FROM tasks WHERE title = '".$title."' AND dateGMT='" . $dateGMT . "'";
        $check = $this->db->prepare($query);
        $check->execute();

        return $check->fetchColumn();
    }

    /**
     * @param $taskID
     * @return array
     */
    public function selectTask($taskID)
    {

        $query = "SELECT * FROM tasks WHERE ID = '".$taskID."'";

        $check = $this->db->prepare($query);
        $check->execute();

        return $check->fetchAll();
    }

    /**
     * @return mixed
     */
    public function selectTasks()
    {
        $query = "SELECT * FROM tasks WHERE status = 0";

        $check = $this->db->prepare($query);
        $check->execute();

        return $check->fetchAll();
    }

    /**
     * @param $taskID
     * @return bool
     */
    public function done($taskID)
    {
        $query = "UPDATE tasks SET status = 1 WHERE ID = '".$taskID."'";

        $check = $this->db->prepare($query);
        return $check->execute();
    }
}
