<?php
namespace App\Controller;

use App\Model\GenerateDB;
use App\Model\MainDB;

class DataLogic
{
    /**
     * @var MainDB
     */
    private $db;

    /**
     * DataLogic constructor.
     */
    public function __construct()
    {
        $this->db = new MainDB();
    }

    /**
     * @return string
     */
    public function uploadFile()
    {
        if ($_FILES["textfile"]["type"] !== 'text/plain') {
            return "Файл должен быть в .txt формате";
        }

        if (move_uploaded_file($_FILES["textfile"]["tmp_name"], 'uploads.txt')) {
            return $this->addTasksStatus();
        } else {
            return "Ошибка при загрузке файла.";
        }
    }

    /**
     * @return string
     */
    public function addTasksStatus()
    {
        $tasks = file('uploads.txt');
        $data = [];

        foreach ($tasks AS $task) {
            $semiExp = explode(';', $task);
            preg_match('/(^\S*)\s(.*?\d)\s(.*)$/m', $semiExp[0], $matches);

            if ($matches[0] === '' || $matches[1] === '' || $matches[2] === '' || $semiExp[1] === ''){
                return 'Неверный формат данных в файле';
            }

            $data['cat_name'] = $matches[1];
            $data['dateGMT'] = $matches[2];
            $data['title'] = $matches[3];
            $data['description'] = $semiExp[1];

            if (!$this->db->checkDuplicate($data)) {
                $this->db->addTask($data);
            }
        }

        return 'Таски добавлены!';
    }
}
