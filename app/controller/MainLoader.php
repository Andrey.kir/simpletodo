<?php
namespace App\Controller;

use App\Core\Controller;
use App\Model\MainDB;

class MainLoader extends Controller
{
    /**
     * @return void
     */
    public function actionLoad($isLogin = false, $error = false)
    {

        if ($isLogin) {
            $userStatus = $_SESSION['userStatus'];

            $db = new MainDB();
            $data = $db->selectTasks();

            if ($userStatus === 'admin') {
                $this->view->generate('mainAdmin.php', 'mainTemplate.php', $data, $error);
            } else {
                $this->view->generate('mainUser.php', 'mainTemplate.php', $data);
            }

        } else {
            $this->view->generate('login.php', 'mainTemplate.php', false , $error);
        }
    }
}
