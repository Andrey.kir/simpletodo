<?php
namespace App\Controller;

use App\Model\MainDB;

class AuthLogic
{

    /**
     * @var MainData
     */
    private $model;

    /**
     * AuthLogic constructor.
     */
    public function __construct()
    {
        $this->db = new MainDB();
    }

    /**
     * @return bool
     */
    public function isLogin()
    {
        session_start();
        if ($_SESSION['auth']) {
            return true;
        }
        session_destroy();
        return false;
    }

    /**
     * @return bool|string
     */
    public function login()
    {

        if ($_POST['name'] != "" && $_POST['password'] != "") {

            $userStatus = $this->db->checkUser();

            if ($userStatus) {
                setcookie("login", $_POST['name'], time() + 300);
                session_start();
                $_SESSION['auth'] = true;
                $_SESSION['userStatus'] = $userStatus;
                return false;
            }

            return "Неверные Данные";
        } else {
            return "Поля не должны быть пустыми!";
        }
    }

    /**
     * @return void
     */
    public function logout()
    {
        session_start();
        session_destroy();
    }
}
