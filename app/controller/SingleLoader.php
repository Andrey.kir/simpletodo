<?php
namespace App\Controller;

use App\Core\Controller;
use App\Model\MainDB;

class SingleLoader extends Controller
{
    /**
     * @return void
     */
    public function actionLoad($isLogin = false, $error = false, $taskID = false)
    {

        if ($isLogin) {
            $db = new MainDB();
            $data = $db->selectTask($taskID)[0];

            $this->view->generate('singleTask.php', 'mainTemplate.php', $data);
        } else {
            $this->view->generate('login.php', 'mainTemplate.php');
        }
    }
}