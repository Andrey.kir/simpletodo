<?php
namespace App\Controller;

use App\Core\Controller;

class Error404Loader extends Controller
{
    /**
     * @return void
     */
    function actionLoad($isLogin = false, $loginError = false)
    {
            $this->view->generate('error404.php', 'mainTemplate.php');
    }
}
