<?php
namespace App\Core;

use App\Core\View;

abstract class Controller
{
    /**
     * @var View
     */
    protected $view;

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        $this->view = new View();
    }

    /**
     * @return void
     */
    public function actionLoad($isLogin = false, $loginError = false)
    {}
}
