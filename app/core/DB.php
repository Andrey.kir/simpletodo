<?php
namespace App\Core;
use PDO;

class  DB{
    private $dsn = 'mysql:host=localhost;dbname=todolist';
    private $user = 'root';
    private $pwd = 'root';
    private $opt = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES => false,
    ];

    /**
     * @return PDO
     */
    public function connect(){
        $connection = new PDO($this->dsn, $this->user, $this->pwd, $this->opt);

        return $connection;
    }
}
