<?php

use App\Controller\AuthLogic;
use App\Controller\DataLogic;
use App\Controller\MainLoader;
use App\Controller\SingleLoader;
use App\Controller\Error404Loader;
use App\Model\MainDB;

class Route
{
    /**
     * @return void
     */
    static public function start()
    {
        $error = false;
        $auth = new AuthLogic();

        if (isset($_POST['via']) && $_POST['via'] == 'logout') {
            $auth->logout();
        }

        $isLogin = $auth->isLogin();

        if (isset($_POST['via']) && $_POST['via'] == 'login') {
            $error = $auth->login();
            if ($error === false) {
                $isLogin = true;
            }
        }

        if (isset($_FILES) && isset($_POST['via']) && $_POST['via'] == 'upload') {
            $upload = new DataLogic();
            $error = $upload->uploadFile();
        }

        $routes = explode('/', $_SERVER['REQUEST_URI']);

        if (!empty($routes[1])) {
            $baseControllerName = $routes[1];

            if ($baseControllerName == "error404") {
                $page404Loader = new Error404Loader();
                $page404Loader->actionLoad();
            } else {
                if ($baseControllerName == "tasks") {
                    $taskID = $routes[2];

                    if($taskID) {
                        if (isset($_POST['via']) && $_POST['via'] == 'done') {
                            $db = new MainDB();
                            $db->done($taskID);
                        }
                        self::singlePage($isLogin, $error, $taskID);
                    } else {
                        self::pageMain($isLogin, $error);
                    }

                } else {
                    self::page404();
                }
            }
        } else {
            self::pageMain($isLogin, $error);
        }
    }

    /**
     * @return void
     */
    static private function page404()
    {
        $host = 'http://' . $_SERVER['HTTP_HOST'] . '/';
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
        header('Location:' . $host . 'error404');
    }

    /**
     * @param $isLogin
     * @param $loginError
     */
    static private function pageMain($isLogin, $error)
    {
        $mainLoader = new MainLoader();
        $mainLoader->actionLoad($isLogin, $error);
    }

    /**
     * @param $isLogin
     * @param $loginError
     * @param $taskID
     */
    static private function singlePage($isLogin, $error, $taskID)
    {
        $mainLoader = new SingleLoader();
        $mainLoader->actionLoad($isLogin, $error, $taskID);
    }
}
